// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

namespace UnrealBuildTool.Rules
{
	public class AGUArtGraphicUtils : ModuleRules
	{
		public AGUArtGraphicUtils(ReadOnlyTargetRules Target) : base(Target)
		{
			PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
			bEnforceIWYU = true;

            PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"ApplicationCore",
					"AssetManagerEditor",
					"AssetRegistry",
					"AssetTools",
					"Blutility",
					"Core",
					"CoreUObject",
					"EditorStyle",
					"Engine",
					"Json",
					"LevelEditor",
					"MessageLog",
					"MovieScene",
                    "MovieSceneTracks",
                    "Projects",
					"Slate",
					"SlateCore",
					"SourceControl",
					"StatsViewer",
					"UMG",
					"UMGEditor",
					"UnrealEd",

                    "MongoDB",

                    "PythonScriptPlugin",
                }
			);
		}
	}
}
