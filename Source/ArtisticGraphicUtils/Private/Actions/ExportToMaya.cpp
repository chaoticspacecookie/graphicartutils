// Created by kukki. 13 July 2021.

#include "ExportToMaya.h"

#include "Utilities/FAMExport.h"
#include "IAGUActionRegistryModule.h"
#include "Iterators/AssetRegistryIteration.h"
#include "IAGUArtGraphicUtils.h"

bool ExportToMaya(const AssetRegistryIterator::FIterableAssets& InSequences)
{
	auto Sequencer = IAGUActionRegistryModule::Get().GetLevelSequenceActionRegistry().GetSequencerHack();
	if (Sequencer.IsValid())
	{
		//IAGUFacialPipelineUI::Get().OnExportSequenceDescription(Sequencer);
		FAMExport::ExportSequenceDescription(Sequencer);
		return true;
	}
	else
	{
		UE_LOG(LogAGUArtGraphicUtils, Error, TEXT("Could not get Sequencer"));
	}

	return false;
}