// Created by kukki. 13 July 2021.
#pragma once

namespace AssetRegistryIterator {
	// Forward declarations
	class FIterableAssets;
} // namespace AssetRegistryIterator

bool ExportToMaya(const AssetRegistryIterator::FIterableAssets& InSequences);