// Created by kukki. 13 July 2021.

#include "ImportFromMaya.h"

#include "AnimationImporter.h"
#include "IAGUActionRegistryModule.h"
#include "IAGUFacialPipelineUI.h"
#include "MovieSceneSection.h"
#include "Animation/AnimSequenceBase.h"
#include "Sequencer/Sections/MovieSceneAGUPerfCapSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapAnimationSubSection.h"

namespace
{
	TArray<UAnimSequenceBase*> ExtractBodyAnimationFromSection(UMovieSceneSection* InSection, FName InSlotName)
	{
		TArray<UAnimSequenceBase*> Tmp;
		if (auto Section = Cast<UMovieSceneAGUPerfCapSection>(InSection))
		{
			if (auto GameSubSection = Section->GetGameSubSection<UMovieSceneAGUPerfCapAnimationSubSection>())
			{
				if (GameSubSection->SlotName == InSlotName)
				{
					for(auto& Chunk : GameSubSection->Chunks)
					{
						Tmp.AddUnique(Chunk.Animation);
					}
				}
			}
		}
		return Tmp;
	}
}

bool ImportFromMaya(const AssetRegistryIterator::FIterableAssets& InSequences)
{
	auto Sequencer = IAGUActionRegistryModule::Get().GetLevelSequenceActionRegistry().GetSequencerHack();

	if (Sequencer.IsValid())
	{
		FacialPipelineProcess::FScopedSectionToDisableHook Hook([](UMovieSceneSection* InSection, FName InSlot) { return ExtractBodyAnimationFromSection(InSection, InSlot); });
		IAGUFacialPipelineUI::Get().OnImportSeqList2(Sequencer);
		return true;
	}
	else
	{
		UE_LOG(LogAGUPerformanceCapture, Error, TEXT("Could not get Sequencer"));
	}

	return false;
}