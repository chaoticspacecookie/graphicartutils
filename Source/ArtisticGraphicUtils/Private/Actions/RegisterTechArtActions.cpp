// Created by kukki. 13 July 2021.

#include "RegisterTechArtActions.h"

#include "IAGUActionRegistryModule.h"
#include "Actions/LevelSequenceAGUAction.h"

#include "ExportToMaya.h"
#include "ImportFromMaya.h"
#include "AGUArtGraphicUtilsStyle.h"

namespace RegisterActionsImpl
{
	FSlateIcon MakeIcon(FName InLarge, FName InSmall = NAME_None)
	{
		static const auto StyleSetName = FAGUArtGraphicUtilsStyle::GetStyleSetName();
		return FSlateIcon(StyleSetName, InLarge, InSmall == NAME_None ? InLarge : InSmall);
	}
}

namespace AGUArtGraphicUtils {

	void RegisterActions()
	{
		using namespace RegisterActionsImpl;

		auto& AGUActionRegistryModule = IAGUActionRegistryModule::Get();
		auto& Registry = AGUActionRegistryModule.GetLevelSequenceActionRegistry();

		using namespace AGUActionRegistry;
		using	FOptions = FLevelSequenceAGUAction::FOptions;
		using	EActionScope = FLevelSequenceAGUAction::EActionScope;
		using	EActionRequirements = FLevelSequenceAGUAction::EActionRequirements;

		auto	SequencerHackOptions = FOptions();
		SequencerHackOptions.Scope = EActionScope::Single;
		SequencerHackOptions.Requirements = { EActionRequirements::SequencerHack };

		auto	ArtCategory = Registry.CreateCategory("Maya", MakeIcon("Actions.Maya"));

		Registry.RegisterAction(ArtCategory, "Export to Maya", FOnExecuteLevelSequenceAGUAction::CreateStatic(ExportToMaya), SequencerHackOptions, MakeIcon("Actions.Maya"));
		Registry.RegisterAction(ArtCategory, "Import from Maya (seqlist2)", FOnExecuteLevelSequenceAGUAction::CreateStatic(ImportFromMaya), SequencerHackOptions, MakeIcon("Actions.Maya"));
	}

} // namespace AGUArtGraphicUtils