// Created by kukki, 07 July 2021.

#include "IAGUArtGraphicUtils.h"

#include "ARFilter.h"
#include "AssetData.h"
#include "AssetRegistryModule.h"
#include "AssetToolsModule.h"
#include "Blutility/Classes/EditorUtilityWidget.h"
#include "Blutility/Classes/EditorUtilityWidgetBlueprint.h"
#include "Editor.h"
#include "EditorUtilitySubsystem.h"
#include "EditorUtilityWidgetBlueprint.h"
#include "Framework/Commands/UICommandList.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"
#include "Framework/MultiBox/MultiBoxExtender.h"
#include "IAssetRegistry.h"
#include "Interfaces/IPluginManager.h"
#include "LevelEditor.h"
#include "ObjectEditorUtils.h"
#include "WidgetBlueprint.h"
#include "Actions/RegisterTechArtActions.h"
#include "IPythonScriptPlugin.h"

#if WITH_EDITORONLY_DATA
#include "ISettingsModule.h"
#include "AGUArtGraphicUtilsSettings.h"
#endif // WITH_EDITORONLY_DATA

#include "AGUArtGraphicUtilsStyle.h"

DEFINE_LOG_CATEGORY(LogAGUArtGraphicUtils);

#define LOCTEXT_NAMESPACE "FAGUArtGraphicUtils"

class FAGUArtGraphicUtils : public IAGUArtGraphicUtils
{
public:

	void					StartupModule() final;
	void					ShutdownModule() final;

private:

	void					SetMenuExtensionBlueprintsFilter();
	void					GatherMenuExtensionBlueprints();
	void					AddMenuExtensionBlueprints(FMenuBuilder& InMenuBuilder);

	void					AddMenuExtension();
	void					RemoveMenuExtension();

	void					LoadPythonScripts();
	void					RegisterSettings();
	void					UnRegisterSettings();

private:

	IAssetRegistry*			AssetRegistry;
	TSharedPtr<FExtender>	MenuExtension;
	TArray<FAssetData>		MenuExtensionBlueprints;
	FARFilter				MenuExtensionBlueprintsFilter;
};

IMPLEMENT_MODULE(FAGUArtGraphicUtils, AGUArtGraphicUtils)

void FAGUArtGraphicUtils::StartupModule()
{
	RegisterSettings();

	FAGUArtGraphicUtilsStyle::Initialize();

	AssetRegistry = &FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry")).Get();

	AGUArtGraphicUtils::RegisterActions();

	SetMenuExtensionBlueprintsFilter();
	AddMenuExtension();

	LoadPythonScripts();
}

void FAGUArtGraphicUtils::ShutdownModule()
{
	UnRegisterSettings();

	RemoveMenuExtension();

	FAGUArtGraphicUtilsStyle::Shutdown();
}

void FAGUArtGraphicUtils::RegisterSettings()
{
#if WITH_EDITORONLY_DATA
	if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		SettingsModule->RegisterSettings("Project", "AGU Plugins", "AGU Settings",
			LOCTEXT("AGUArtGraphicUtilsSettings", "AGU Tech Art Tools Settings"),
			LOCTEXT("AGUArtGraphicUtilsSettings", "AGU Tech Art Tools Settings"),
			GetMutableDefault<UAGUArtGraphicUtilsSettings>()
		);
	}
#endif
}

void FAGUArtGraphicUtils::UnRegisterSettings()
{
#if WITH_EDITORONLY_DATA
	if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		SettingsModule->UnregisterSettings("Project", "AGU Plugins", "AGU Settings");
	}
#endif
}

void FAGUArtGraphicUtils::LoadPythonScripts()
{
	if (IPythonScriptPlugin::Get()->IsPythonAvailable())
	{
		for (const FString& StartupScript : UAGUArtGraphicUtilsSettings::Get()->StartupScripts)
		{
			UE_LOG(LogTemp, Display, TEXT("Python script %s"), *StartupScript);
			IPythonScriptPlugin::Get()->ExecPythonCommand(*StartupScript);
		}
	}
}

void FAGUArtGraphicUtils::SetMenuExtensionBlueprintsFilter()
{
	MenuExtensionBlueprintsFilter.ClassNames.Add(UEditorUtilityWidgetBlueprint::StaticClass()->GetFName());
	MenuExtensionBlueprintsFilter.PackagePaths.Add(TEXT("/AGUArtGraphicUtils"));
	MenuExtensionBlueprintsFilter.bRecursivePaths	= false;
	MenuExtensionBlueprintsFilter.bRecursiveClasses	= true;
}

void FAGUArtGraphicUtils::GatherMenuExtensionBlueprints()
{
	if (AssetRegistry->IsLoadingAssets())
	{
		return;
	}

	AssetRegistry->GetAssets(MenuExtensionBlueprintsFilter, MenuExtensionBlueprints);
}

void FAGUArtGraphicUtils::AddMenuExtensionBlueprints(FMenuBuilder& InMenuBuilder)
{
	GatherMenuExtensionBlueprints();

	if (MenuExtensionBlueprints.Num() > 0)
	{
		FAssetToolsModule&				AssetToolsModule	= FModuleManager::LoadModuleChecked<FAssetToolsModule>(TEXT("AssetTools"));
		TSharedPtr<IAssetTypeActions>	AssetActions		= AssetToolsModule.Get().GetAssetTypeActionsForClass(UEditorUtilityWidgetBlueprint::StaticClass()).Pin();

		if (AssetActions.IsValid())
		{
			TArray<UObject*> Items;

			for (FAssetData& Asset : MenuExtensionBlueprints)
			{
				int32 Index = Items.AddUnique(Asset.GetAsset());
				Items[Index]->SetFlags(RF_Standalone); // Should prevent the garbage collector from destroying the extension widget
			}

			AssetActions->GetActions(Items, InMenuBuilder);
		}
	}
}

void FAGUArtGraphicUtils::AddMenuExtension()
{
	FLevelEditorModule* LevelEditorModule = FModuleManager::GetModulePtr<FLevelEditorModule>(TEXT("LevelEditor"));

	if (LevelEditorModule != nullptr)
	{
		auto PluginCommands	= MakeShared<FUICommandList>();
		MenuExtension		= MakeShared<FExtender>();

		MenuExtension->AddMenuExtension("Audio", EExtensionHook::After, PluginCommands, FMenuExtensionDelegate::CreateLambda(
		[this](FMenuBuilder& InMenuBuilder)
		{
			InMenuBuilder.BeginSection(TEXT("Tech Art Tools"), FText::FromString("Tech Art Tools"));
			AddMenuExtensionBlueprints(InMenuBuilder);
			InMenuBuilder.EndSection();
		}));

		LevelEditorModule->GetMenuExtensibilityManager()->AddExtender(MenuExtension);
	}
}

void FAGUArtGraphicUtils::RemoveMenuExtension()
{
	FLevelEditorModule* LevelEditorModule = FModuleManager::GetModulePtr<FLevelEditorModule>(TEXT("LevelEditor"));

	if (LevelEditorModule != nullptr && MenuExtension.IsValid())
	{
		LevelEditorModule->GetMenuExtensibilityManager()->RemoveExtender(MenuExtension);
	}
}

#undef LOCTEXT_NAMESPACE