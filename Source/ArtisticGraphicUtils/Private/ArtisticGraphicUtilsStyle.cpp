// Created by kukki. 13 July 2021.

#include "AGUArtGraphicUtilsStyle.h"

#include "Styling/SlateStyle.h"
#include "Styling/SlateStyleRegistry.h"
#include "Templates/UniquePtr.h"

namespace
{
	const FString& ContentDir()
	{
		static const FString Directory = FPaths::EnginePluginsDir() / TEXT("AGU") / TEXT("AGUArtGraphicUtils") / TEXT("Content") / TEXT("");

		return Directory;
	}

	TSharedPtr<FSlateStyleSet> StyleSet;
}

TSharedPtr<ISlateStyle> FAGUArtGraphicUtilsStyle::Get()
{
	return StyleSet;
}

FName FAGUArtGraphicUtilsStyle::GetStyleSetName()
{
	static const FName Name(TEXT("AGUArtGraphicUtils"));

	return Name;
}

void FAGUArtGraphicUtilsStyle::Initialize()
{
	if (StyleSet.IsValid())
	{
		return;
	}

	StyleSet = MakeShared<FSlateStyleSet>(GetStyleSetName());
	StyleSet->SetContentRoot(FPaths::EngineContentDir() / TEXT("Editor/Slate"));
	StyleSet->SetCoreContentRoot(FPaths::EngineContentDir() / TEXT("Slate"));

	// Icons
	{
		StyleSet->Set("Actions.Maya", new FSlateImageBrush(ContentDir() / "Maya.png", FVector2D(16.f, 16.f)));
	}

	FSlateStyleRegistry::RegisterSlateStyle(*StyleSet.Get());
}

void FAGUArtGraphicUtilsStyle::Shutdown()
{
	if (!StyleSet.IsValid())
	{
		return;
	}

	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleSet.Get());
	ensure(StyleSet.IsUnique());
	StyleSet.Reset();
}
