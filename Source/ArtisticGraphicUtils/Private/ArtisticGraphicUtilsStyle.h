// Created by kukki. 13 July 2021.
#pragma once
#include "Styling/ISlateStyle.h"
#include "Templates/SharedPointer.h"
#include "UObject/NameTypes.h"

class FAGUArtGraphicUtilsStyle
{
public:

	static void                    Initialize();
	static void                    Shutdown();

	static TSharedPtr<ISlateStyle> Get();
	static FName                   GetStyleSetName();
};
