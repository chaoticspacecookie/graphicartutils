// Created by kukki, 07 July 2021.
#include "Textures/AGUTextureStats.h"
#include "ARFilter.h"
#include "AssetData.h"
#include "AssetRegistryModule.h"
#include "Engine/Texture.h"
#include "Engine/Texture2D.h"
#include "Engine/TextureCube.h"
#include "IAssetRegistry.h"
#include "IAGUArtGraphicUtils.h"
#include "Modules/ModuleManager.h"

namespace AGUTextureStats
{
	class FAssetQuery
	{
	public:

		FAssetQuery()
			: AssetRegistry(FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry")).Get())
			, AssetFilter()
			, TextureAssets()
		{
			AssetFilter.ClassNames.Add(UTexture::StaticClass()->GetFName());
			AssetFilter.bRecursiveClasses = true;
		}

		FARFilter& GetFilter()
		{
			return AssetFilter;
		}

		void RunQuery()
		{
			AssetRegistry.GetAssets(AssetFilter, TextureAssets);
		}

		UTexture* GetTexture() const
		{
			static const auto NamesToStrings = [](const TArray<FName> InNames)
			{
				TArray<FString> Values;
				for (const FName& In : InNames)
				{
					Values.Emplace(In.ToString());
				}
				return Values;
			};

			if (TextureAssets.Num() == 0)
			{
				FString Query = TEXT("<unknown>");
				FString Value = TEXT("<unknown>");

				if (AssetFilter.PackageNames.Num() > 0)
				{
					Query = TEXT("package name(s)");
					Value = FString::Join(NamesToStrings(AssetFilter.PackageNames), TEXT(", "));
				}
				else if (AssetFilter.ObjectPaths.Num() > 0)
				{
					Query = TEXT("object path(s)");
					Value = FString::Join(NamesToStrings(AssetFilter.ObjectPaths), TEXT(", "));
				}

				UE_LOG(LogAGUArtGraphicUtils, Warning, TEXT("Failed to find asset(s) using %s %s"), *Query, *Value);
			}

			return TextureAssets.Num() > 0 ? FAssetData::GetFirstAsset<UTexture>(TextureAssets) : nullptr;
		}

	private:

		IAssetRegistry&		AssetRegistry;
		FARFilter			AssetFilter;
		TArray<FAssetData>	TextureAssets;
	};

	FString GetTexturePath(const FString& InFullyQualifiedPath)
	{
		const int32 Index = InFullyQualifiedPath.Find(TEXT("."));

		return Index != INDEX_NONE ? InFullyQualifiedPath.Left(Index) : FString();
	}
}

UAGUTextureStatsData* UAGUTextureStats::GetTextureStatsByPackageName(FString InPackageName)
{
	AGUTextureStats::FAssetQuery Query;

	Query.GetFilter().PackageNames.Add(*InPackageName);
	Query.RunQuery();

	UTexture* Texture = Query.GetTexture();

	return Texture != nullptr ? GetTextureStats(Texture) : nullptr;
}

UAGUTextureStatsData* UAGUTextureStats::GetTextureStatsByObjectPath(FString InObjectPath)
{
	AGUTextureStats::FAssetQuery Query;

	Query.GetFilter().ObjectPaths.Add(*InObjectPath);
	Query.RunQuery();

	UTexture* Texture = Query.GetTexture();

	return Texture != nullptr ? GetTextureStats(Texture) : nullptr;
}

UAGUTextureStatsData* UAGUTextureStats::GetTextureStatsByStringReference(FSoftObjectPath InReference)
{
	if (!InReference.IsValid())
	{
		UE_LOG(LogAGUArtGraphicUtils, Warning, TEXT("Invalid soft object path %s"), *InReference.ToString());

		return nullptr;
	}

	UObject* ReferencedObject = InReference.ResolveObject();

	if (ReferencedObject == nullptr)
	{
		ReferencedObject = InReference.TryLoad();
	}

	if (ReferencedObject == nullptr)
	{
		UE_LOG(LogAGUArtGraphicUtils, Warning, TEXT("Could not load object from soft object path %s"), *InReference.ToString());

		return nullptr;
	}

	if (UTexture* Texture = Cast<UTexture>(ReferencedObject))
	{
		return GetTextureStats(Texture);
	}

	if (const UPackage* Package = Cast<UPackage>(ReferencedObject))
	{
		return GetTextureStatsByPackageName(Package->GetName());
	}

	UE_LOG(LogAGUArtGraphicUtils, Warning, TEXT("Could not convert object to texture or package from soft object path %s"), *InReference.ToString());

	return nullptr;
}

UAGUTextureStatsData* UAGUTextureStats::GetTextureStats(UTexture* InTexture)
{
	if (InTexture == nullptr)
	{
		UE_LOG(LogAGUArtGraphicUtils, Warning, TEXT("Invalid texture object"));

		return nullptr;
	}

	UAGUTextureStatsData*	Entry		= NewObject<UAGUTextureStatsData>();
	const FTexture*			Resource	= InTexture->Resource;
	const UTexture2D*		Texture2D	= Cast<const UTexture2D>(InTexture);
	const UTextureCube*		TextureCube	= Cast<const UTextureCube>(InTexture);

	Entry->Texture			= MakeWeakObjectPtr(InTexture);
	Entry->Path				= AGUTextureStats::GetTexturePath(InTexture->GetPathName());
	Entry->Group			= (TextureGroup)InTexture->LODGroup;
	Entry->CurrentKB		= InTexture->CalcTextureMemorySizeEnum(TMC_ResidentMips) / 1024.0f;
	Entry->FullyLoadedKB	= InTexture->CalcTextureMemorySizeEnum(TMC_AllMipsBiased) / 1024.0f;
	Entry->LODBias			= InTexture->GetCachedLODBias();
	Entry->LastTimeRendered	= StaticCast<float>(FMath::Max(Resource != nullptr ? FApp::GetLastTime() - Resource->LastRenderTime : 0.0, 0.0));

	if (Texture2D != nullptr)
	{
		Entry->Format	= Texture2D->GetPixelFormat();
		Entry->Type		= TEXT("2D");

		// Calculate in game current dimensions
		const int32 DroppedMips	= Texture2D->GetNumMips() - Texture2D->GetNumResidentMips();
		Entry->CurrentDim.X		= Texture2D->GetSizeX() >> DroppedMips;
		Entry->CurrentDim.Y		= Texture2D->GetSizeY() >> DroppedMips;

		// Calculate the max dimensions
		Entry->MaxDim.X = Texture2D->GetSizeX() >> Entry->LODBias;
		Entry->MaxDim.Y = Texture2D->GetSizeY() >> Entry->LODBias;
	}
	else if (TextureCube != nullptr)
	{
		Entry->Format	= TextureCube->GetPixelFormat();
		Entry->Type		= TEXT("Cube");

		// Calculate in game current dimensions
		Entry->CurrentDim.X = TextureCube->GetSizeX() >> Entry->LODBias;
		Entry->CurrentDim.Y = TextureCube->GetSizeY() >> Entry->LODBias;

		// Calculate the max dimensions
		Entry->MaxDim.X = TextureCube->GetSizeX() >> Entry->LODBias;
		Entry->MaxDim.Y = TextureCube->GetSizeY() >> Entry->LODBias;
	}

	return Entry;
}

UTexture* UAGUTextureStatsData::GetTexture() const
{
	return Texture.IsValid(false) ? Texture.Get(false) : nullptr;
}
