// Created by kukki, 07 July 2021.

#include "FAMExport.h"
#include "IAGUArtGraphicUtils.h"
#include "EditorFramework/AssetImportData.h"
#include "FileTimeSlice.h"
#include "SequenceName.h"
#include "AssetRegistryModule.h"
#include "IAssetRegistry.h"
#include "SequenceUtils.h"
#include "LevelSequence/LevelSequenceAGU.h"
#include "MovieScene.h"
#include "Tracks/RawMovie/MovieSceneRawMovieTrack.h"
#include "Tracks/RawAudio/MovieSceneRawAudioTrack.h"
#include "Tracks/RawMedia/MovieSceneRawMediaTrack.h"
#include "Tracks/RawMedia/MovieSceneRawMediaSection.h"
#include "Sections/MovieSceneSkeletalAnimationSection.h"
#include "Tracks/SkeletalAnimationAGU/MovieSceneSkeletalAnimationAGUTrack.h"
#include "Tracks/SkeletalAnimationAGU/MovieSceneSkeletalAnimationAGUSection.h"
#include "Tracks/BodyShootCut/MovieSceneBodyShootCutTrack.h"
#include "Tracks/BodyShootCut/MovieSceneBodyShootCutSection.h"
#include "Camera/ICameraAGUInterface.h"
#include "Camera/CineCameraComponentAGU.h"
#include "Tracks/MovieSceneFloatTrack.h"
#include "Tracks/MovieScene3DTransformTrack.h"
#include "Sections/MovieScene3DTransformSection.h"
#include "Tracks/MovieSceneCameraCutTrack.h"
#include "Sections/MovieSceneCameraCutSection.h"
#include "AGUSequencerUtils.h"
#include "Tracks/MovieSceneSubTrack.h"
#include "Sections/MovieSceneSubSection.h"
#include "GameFramework/Actor.h"
#include "Tracks/AnimPlacement/MovieSceneAGUAnimPlacementTrack.h"
#include "Tracks/AnimPlacement/MovieSceneAGUAnimPlacementSection.h"
#include "MovieSceneSection.h"
#include "Channels/MovieSceneChannelProxy.h"
#include "Serialization/JsonSerializer.h"
#include "Serialization/JsonWriter.h"
#include "Misc/FileHelper.h"
#include "ISequencer.h"
#include "Sequencer/TrackEditors/MovieSceneAGUPerfCapTrackEditor.h"
#include "Sequencer/Tracks/MovieSceneAGUPerfCapTrack.h"
#include "Sequencer/Sections/MovieSceneAGUPerfCapSection.h"
#include "Sequencer/Sections/MovieSceneAGUReferenceSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapDbMediaSubSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapRawAudioSubSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapRawVideoSubSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapTrimmerSubSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapGameAudioSubSection.h"
#include "Sequencer/SubSections/MovieSceneAGUPerfCapAnimationSubSection.h"
#include "BodyShootDatabase.h"
#include "Dom/JsonObject.h"
#include "Misc/MessageDialog.h"
#include "Animation/AnimSequence.h"
#include "MongoDb/DocumentVisitor.h"
#include "Utilities/MongoDbHelpers.h"
#include "Utilities/MediaDatabase.h"
#include "SourceControlHelpers.h"

namespace FAMExport
{
	namespace Internals
	{
		struct AdditiveValues
		{
			FVector Translation;
			FRotator Rotation;
			FVector Scale;
		};

		TArray<AdditiveValues> getAdditiveTrackValues(UMovieSceneTrack* InTrack, FFrameTime FrameTime,  float InTime)
		{
			TArray<AdditiveValues> results;
			for (auto Section : InTrack->GetAllSections())
			{
				
				if (Section->GetBlendType() == EMovieSceneBlendType::Additive)
				{
					FFrameNumber Frame = InTrack->GetTypedOuter<UMovieScene>()->GetTickResolution().AsFrameNumber(InTime);
					if (Section->GetRange().Contains(Frame))
					{
						auto& result = results.Emplace_GetRef();
						
						if (auto AdditiveSection = Cast<UMovieScene3DTransformSection>(Section))
						{
							TArrayView<FMovieSceneFloatChannel*> Channels = AdditiveSection->GetChannelProxy().GetChannels<FMovieSceneFloatChannel>();

							Channels[0]->Evaluate(FrameTime, result.Translation.X);
							Channels[1]->Evaluate(FrameTime, result.Translation.Y);
							Channels[2]->Evaluate(FrameTime, result.Translation.Z);

							Channels[3]->Evaluate(FrameTime, result.Rotation.Roll);
							Channels[4]->Evaluate(FrameTime, result.Rotation.Pitch);
							Channels[5]->Evaluate(FrameTime, result.Rotation.Yaw);

							Channels[6]->Evaluate(FrameTime, result.Scale.X);
							Channels[7]->Evaluate(FrameTime, result.Scale.Y);
							Channels[8]->Evaluate(FrameTime, result.Scale.Z);/**/
						}
					}
				}
			}

			return results;
		};
		
		FString CorrectGetOriginalSourceAnimationFile(FString InSectionId, FString InMediaId)
		{
			using namespace AGUPerformanceCapture;

			InSectionId.ReplaceInline(TEXT("-"), TEXT(""));
			InMediaId.ReplaceInline(TEXT("-"), TEXT(""));

			auto OrderedMedia = FMediaDatabase::Get().WaitUntilLoaded().GetOrderedMediaByMediaId(InMediaId).FilterByPredicate([&InSectionId](const FMediaDatabase::FOrderedMedia& In)
			{
				return In.Orders.FindByPredicate([&InSectionId](const FMediaDatabase::FOrderedMediaItem& Inner) { return Inner.Sections.Contains(InSectionId); }) != nullptr;
			});

			if (OrderedMedia.Num() == 0)
			{
				return {};
			}

			auto LatestItem	= OrderedMedia.Pop().Orders.Pop();
			auto FileName	= LatestItem.TargetFilename.TrimStartAndEnd();
			auto FilePath	= LatestItem.Path.TrimStartAndEnd();

			if (!FileName.IsEmpty() && !FilePath.IsEmpty())
			{
				if (FilePath.Find(TEXT("/")) != INDEX_NONE && !FilePath.EndsWith(TEXT("/")))
				{
					FilePath.Append(TEXT("/"));
				}

				if (FilePath.Find(TEXT("\\")) != INDEX_NONE && !FilePath.EndsWith(TEXT("\\")))
				{
					FilePath.Append(TEXT("\\"));
				}

				return FilePath + FileName;
			}

			return {};
		}

		FString HackyGetOriginalSourceAnimationFile(FString InMediaId, const FString& InFilename)
		{
			// This hack fixes (incorrectly) a situation where the section has a zero-ed id.
			// It relies on the fact that the filename will match the original filename (excluding the extension). Just the paths differ. I hope!

			using namespace AGUPerformanceCapture;

			auto Filename = FPaths::GetBaseFilename(InFilename).ToLower();
			InMediaId.ReplaceInline(TEXT("-"), TEXT(""));

			auto OrderedMedia = FMediaDatabase::Get().WaitUntilLoaded().GetOrderedMediaByMediaId(InMediaId).FilterByPredicate([&Filename](const FMediaDatabase::FOrderedMedia& In)
			{
				return FPaths::GetBaseFilename(In.Orders[0].TargetFilename.TrimStartAndEnd()).ToLower() == Filename;
			});

			if (OrderedMedia.Num() == 0)
			{
				return {};
			}

			auto LatestItem = OrderedMedia.Pop().Orders.Pop();
			auto FileName = LatestItem.TargetFilename.TrimStartAndEnd();
			auto FilePath = LatestItem.Path.TrimStartAndEnd();

			if (!FileName.IsEmpty() && !FilePath.IsEmpty())
			{
				if (FilePath.Find(TEXT("/")) != INDEX_NONE && !FilePath.EndsWith(TEXT("/")))
				{
					FilePath.Append(TEXT("/"));
				}

				if (FilePath.Find(TEXT("\\")) != INDEX_NONE && !FilePath.EndsWith(TEXT("\\")))
				{
					FilePath.Append(TEXT("\\"));
				}

				return FilePath + FileName;
			}

			return {};
		}

		FString GetOriginalSourceAnimationFile(const FString& InSectionId, const FString& InMediaId, const FString& InFilename)
		{
			FString Result = CorrectGetOriginalSourceAnimationFile(InSectionId, InMediaId);
			if (Result.IsEmpty())
			{
				Result = HackyGetOriginalSourceAnimationFile(InMediaId, InFilename);
			}
			return Result;
		}

		void ShowErrorDialog(const FString& InMessage)
		{
			static const auto Title = FText::FromString(TEXT("An error occurred"));

			FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(InMessage), &Title);
		}

		struct FTrack
		{
			FTrack() = default;
			FTrack(UMovieSceneTrack* InTrack, bool InIsActive) : Track(InTrack), IsActive(InIsActive) {}

			UMovieSceneTrack*	Track;
			bool				IsActive;
		};

		FString MakeRelToAGURoot(const FString& InFilename)
		{
			static FString AGURoot = TEXT("%AGUROOT%");
			static const auto RecursiveReplace = [](FString& InValue, const TCHAR* InFind, const TCHAR* InReplace)
			{
				while (InValue.Contains(InFind, ESearchCase::CaseSensitive))
				{
					InValue.ReplaceInline(InFind, InReplace, ESearchCase::CaseSensitive);
				}
			};
			static FString AGURootExpanded = []()
			{
				FString Value;
				FPlatformMisc::ExpandEnvironmentStr(*AGURoot, Value);
				return Value;
			}();

			if (InFilename.StartsWith(AGURootExpanded, ESearchCase::IgnoreCase))
			{
				FString Path = AGURoot / InFilename.RightChop(AGURootExpanded.Len());

				FPaths::MakePlatformFilename(Path);
				RecursiveReplace(Path, TEXT("//"), TEXT("/"));
				RecursiveReplace(Path, TEXT("\\\\"), TEXT("\\"));

				return Path;
			}

			return InFilename;
		}

		bool IsCameraBinding(const UClass* InClass)
		{
			return FAGUSequencerUtils::IsCameraBinding(InClass);
		}

		bool IsCameraComponent(const UClass* InClass)
		{
			return UCineCameraComponentAGU::StaticClass() == InClass;
		}

		TSharedPtr<FJsonObject> TransformToJson(const FVector& InTranslation, const FQuat& InRotation, const FVector& InScale)
		{
			TSharedRef<FJsonObject> JsonTransform = MakeShared<FJsonObject>();

			if (!InTranslation.ContainsNaN())
			{
				TArray<TSharedPtr<FJsonValue>> JsonTranslate;
				JsonTranslate.Add(MakeShared<FJsonValueNumber>(InTranslation.X));
				JsonTranslate.Add(MakeShared<FJsonValueNumber>(InTranslation.Y));
				JsonTranslate.Add(MakeShared<FJsonValueNumber>(InTranslation.Z));
				JsonTransform->SetArrayField(TEXT("Translation"), JsonTranslate);
			}

			if (!InRotation.ContainsNaN())
			{
				TArray<TSharedPtr<FJsonValue>> JsonRotation;
				JsonRotation.Add(MakeShared<FJsonValueNumber>(InRotation.X));
				JsonRotation.Add(MakeShared<FJsonValueNumber>(InRotation.Y));
				JsonRotation.Add(MakeShared<FJsonValueNumber>(InRotation.Z));
				JsonRotation.Add(MakeShared<FJsonValueNumber>(InRotation.W));
				JsonTransform->SetArrayField(TEXT("Rotation"), JsonRotation);
			}

			if (!InScale.ContainsNaN())
			{
				TArray<TSharedPtr<FJsonValue>> JsonScale;
				JsonScale.Add(MakeShared<FJsonValueNumber>(InScale.X));
				JsonScale.Add(MakeShared<FJsonValueNumber>(InScale.Y));
				JsonScale.Add(MakeShared<FJsonValueNumber>(InScale.Z));
				JsonTransform->SetArrayField(TEXT("Scale"), JsonScale);
			}

			return JsonTransform;
		}

		TArray<TSharedPtr<FJsonValue>> WeightToJson(const FMovieSceneFloatChannel& InChannel, const FFrameRate& InTickResolution)
		{
			TArray<TSharedPtr<FJsonValue>> JsonWeights;

			auto Times = InChannel.GetTimes();
			auto Values = InChannel.GetValues();

			auto TimesIter = Times.begin();
			auto ValuesIter = Values.begin();
			for (; TimesIter != Times.end() && ValuesIter != Values.end(); ++TimesIter, ++ValuesIter)
			{
				TSharedPtr<FJsonObject> JsonItem = MakeShared<FJsonObject>();
				JsonItem->SetNumberField(TEXT("Time"), InTickResolution.AsSeconds(TimesIter->Value));
				JsonItem->SetNumberField(TEXT("Values"), ValuesIter->Value);

				JsonWeights.Add(MakeShared<FJsonValueObject>(JsonItem));
			}

			return JsonWeights;
		}

		template<class SectionType>
		SectionType* FindSectionAtTime(UMovieSceneTrack* InTrack, float InTime)
		{
			FFrameNumber Frame = InTrack->GetTypedOuter<UMovieScene>()->GetTickResolution().AsFrameNumber(InTime);
			for (auto Section : InTrack->GetAllSections())
			{
				if (Section->GetRange().Contains(Frame))
				{
					return Cast<SectionType>(Section);
				}
			}

			return nullptr;
		};

		void FindCameraComponentTracks(UMovieScene* InMovieScene, const FMovieSceneBinding& InBinding, TArray<TSharedPtr<FJsonValue>>& InTracks, float InTimeOffset)
		{
			if (InMovieScene == nullptr) return;

			struct FNames
			{
				FNames(const FString& InTrackName, const FString& InPropertyName, const FString& InOutputName) : TrackName(InTrackName), PropertyName(InPropertyName), OutputName(InOutputName) { }

				FString TrackName;		// The track name we're looking for in the first instance
				FString PropertyName;	// The property name to look for as a fallback if the named track doesn't exist
				FString OutputName;		// The name used in the JSON output for the gathered value(s)
			};

			static const TArray<FNames> HandledTrackOrPropertyNames
			{
				{ TEXT("CurrentFocalLength"), TEXT("CurrentFocalLength"), TEXT("CurrentFocalLength") },
				//{ TEXT("FocusSettings.ManualFocusDistance"), TEXT("FocusSettings.ManualFocusDistance"), TEXT("ManualFocusDistance") },
				//{ TEXT("CurrentFocusDistance"), TEXT("CurrentFocusDistance"), TEXT("CurrentFocusDistance") },
			};

			const auto	TickResolution = InMovieScene->GetTickResolution();
			const float	StartTime = TickResolution.AsSeconds(InMovieScene->GetPlaybackRange().GetLowerBoundValue());
			const float	EndTime = TickResolution.AsSeconds(InMovieScene->GetPlaybackRange().GetUpperBoundValue());

			for (const auto& HandledTrackOrPropertyName : HandledTrackOrPropertyNames)
			{
				auto Values = TArray<TSharedPtr<FJsonValue>>();

				for (int i = 0; i < InMovieScene->GetPossessableCount(); ++i)
				{
					FMovieScenePossessable& Possessable = InMovieScene->GetPossessable(i);

					if (Possessable.GetParent() != InBinding.GetObjectGuid() || !IsCameraComponent(Possessable.GetPossessedObjectClass())) continue;

					for (const FMovieSceneBinding& Binding : InMovieScene->GetBindings())
					{
						if (Binding.GetObjectGuid() != Possessable.GetGuid()) continue;

						auto Track = Binding.GetTracks().FindByPredicate([&HandledTrackOrPropertyName](const UMovieSceneTrack* InTrack)
						{
							const auto TrackName = InTrack->GetTrackName().ToString();

							return TrackName.Compare(HandledTrackOrPropertyName.TrackName, ESearchCase::IgnoreCase) == 0 && Cast<UMovieSceneFloatTrack>(InTrack) != nullptr;
						});

						if (Track == nullptr || *Track == nullptr) continue;

						auto FloatTrack = Cast<UMovieSceneFloatTrack>(*Track);
						auto Step = 1.0f / 30.0f;
						auto LastValue = 0.0f;

						for (float Time = StartTime; Time < EndTime; Time += Step)
						{
							auto FrameTime = TickResolution.AsFrameTime(Time);
							auto Section = FloatTrack->FindSection(FrameTime.FloorToFrame());
							auto Value = 0.0f;

							if (Section != nullptr)
							{
								FMovieSceneFloatChannel* Channel = Section->GetChannelProxy().GetChannel<FMovieSceneFloatChannel>(0);

								if (!Channel->Evaluate(FrameTime, Value))
								{
									Value = 0.0f;
								}
							}

							if (FMath::IsNearlyEqual(Time, StartTime) || !FMath::IsNearlyEqual(Value, LastValue))
							{
								TSharedPtr<FJsonObject> JsonTimeValue = MakeShared<FJsonObject>();

								JsonTimeValue->SetNumberField(TEXT("Time"), Time + InTimeOffset);
								JsonTimeValue->SetNumberField(TEXT("Value"), Value);
								JsonTimeValue->SetBoolField(TEXT("Active"), Section->IsActive());

								Values.Add(MakeShared<FJsonValueObject>(JsonTimeValue));
							}

							LastValue = Value;
						}
					}
				}

				if (Values.Num() == 0)
				{
					auto BoundObjects = Cast<UMovieSceneSequence>(InMovieScene->GetOuter())->LocateBoundObjects(InBinding.GetObjectGuid(), nullptr);

					if (BoundObjects.Num() > 0)
					{
						const auto* Actor = Cast<AActor>(BoundObjects[0]);
						const auto* Component = Actor->GetComponentByClass(UCineCameraComponentAGU::StaticClass());
						const auto* Property = FindFProperty<FProperty>(UCineCameraComponentAGU::StaticClass(), *HandledTrackOrPropertyName.PropertyName);
						const auto* PropValue = Component != nullptr && Property != nullptr ? Property->ContainerPtrToValuePtr<float>(Component) : nullptr;

						if (PropValue != nullptr)
						{
							TSharedPtr<FJsonObject> JsonTimeValue = MakeShared<FJsonObject>();

							JsonTimeValue->SetNumberField(TEXT("Time"), StartTime + InTimeOffset);
							JsonTimeValue->SetNumberField(TEXT("Value"), *PropValue);
							JsonTimeValue->SetBoolField(TEXT("Active"), true);

							Values.Add(MakeShared<FJsonValueObject>(JsonTimeValue));
						}
					}
				}

				if (Values.Num() > 0)
				{
					TSharedPtr<FJsonObject> JsonPropertyTrack = MakeShared<FJsonObject>();

					JsonPropertyTrack->SetStringField(TEXT("Property"), HandledTrackOrPropertyName.OutputName);
					JsonPropertyTrack->SetStringField(TEXT("Type"), TEXT("Float"));
					JsonPropertyTrack->SetNumberField(TEXT("StartTime"), StartTime + InTimeOffset);
					JsonPropertyTrack->SetNumberField(TEXT("EndTime"), EndTime + InTimeOffset);
					JsonPropertyTrack->SetArrayField(TEXT("Values"), Values);

					InTracks.Add(MakeShared<FJsonValueObject>(JsonPropertyTrack));
				}
			}
		}

		TArray<TSharedPtr<FJsonValue>> TransformTrackToJson(UMovieScene3DTransformTrack* Track, float StartTime, float EndTime, float InTimeOffset = 0.0f, FTransform InParentTransform = FTransform::Identity)
		{
			float Step = 1.0f / 30.0f;

			FVector LastTranslation;
			FRotator LastRotation;
			FVector LastScale;

			FFrameRate SequenceFrameRate = Track->GetTypedOuter<UMovieScene>()->GetTickResolution();

			TArray<TSharedPtr<FJsonValue>> Values;
			for (float Time = StartTime; Time < EndTime; Time += Step)
			{
				FVector Translation;
				FRotator Rotation;
				FVector Scale;

				FFrameTime FrameTime = SequenceFrameRate.AsFrameTime(Time);
				if (UMovieScene3DTransformSection* Section = FindSectionAtTime<UMovieScene3DTransformSection>(Track, Time))
				{
					TArrayView<FMovieSceneFloatChannel*> Channels = Section->GetChannelProxy().GetChannels<FMovieSceneFloatChannel>();

					Channels[0]->Evaluate(FrameTime, Translation.X);
					Channels[1]->Evaluate(FrameTime, Translation.Y);
					Channels[2]->Evaluate(FrameTime, Translation.Z);

					Channels[3]->Evaluate(FrameTime, Rotation.Roll);
					Channels[4]->Evaluate(FrameTime, Rotation.Pitch);
					Channels[5]->Evaluate(FrameTime, Rotation.Yaw);

					Channels[6]->Evaluate(FrameTime, Scale.X);
					Channels[7]->Evaluate(FrameTime, Scale.Y);
					Channels[8]->Evaluate(FrameTime, Scale.Z);

					if (Track->GetAllSections().Num() > 1)
					{
						auto additveResults = getAdditiveTrackValues(Track, FrameTime, Time);
						if (additveResults.Num() > 0)
						{
							for (auto additiveTrack : additveResults)
							{
								Translation += additiveTrack.Translation;
								Rotation += additiveTrack.Rotation;
								Scale += additiveTrack.Scale;
							}
						}
					}
				}

				if (Translation != LastTranslation ||
					Rotation != LastRotation ||
					Scale != LastScale)
				{
					auto t = FTransform(Rotation.Quaternion(), Translation, Scale) * InParentTransform;
					TSharedPtr<FJsonObject> Trans = TransformToJson(t.GetTranslation(), t.GetRotation(), t.GetScale3D());
					Trans->SetNumberField(TEXT("Time"), Time + InTimeOffset);
					Values.Add(MakeShared<FJsonValueObject>(Trans));
				}

				LastTranslation = Translation;
				LastRotation = Rotation;
				LastScale = Scale;
			}

			return Values;
		}

		TSharedPtr<FJsonObject> FindCameraTransform(UMovieScene* InMovieScene, const FMovieSceneBinding &InBinding, float InTimeOffset)
		{
			FFrameRate SequenceFrameRate = InMovieScene->GetTickResolution();
			float StartTime = SequenceFrameRate.AsSeconds(InMovieScene->GetPlaybackRange().GetLowerBoundValue());
			float EndTime = SequenceFrameRate.AsSeconds(InMovieScene->GetPlaybackRange().GetUpperBoundValue());

			FTransform ActorTransform, ParentTransform;

			auto BoundObjects = Cast<UMovieSceneSequence>(InMovieScene->GetOuter())->LocateBoundObjects(InBinding.GetObjectGuid(), nullptr);
			if (BoundObjects.Num() > 0)
			{
				AActor* Actor = Cast<AActor>(BoundObjects[0]);
				if (Actor)
				{
					ActorTransform = Actor->GetActorTransform();
					auto ParentActor = Actor->GetAttachParentActor();
					auto SocketName = Actor->GetAttachParentSocketName();

					if (ParentActor && ParentActor->GetRootComponent())
					{
						ParentTransform = ParentActor->GetRootComponent()->GetSocketTransform(SocketName, RTS_World);
					}
				}
			}

			UMovieScene3DTransformTrack* Track = InMovieScene->FindTrack<UMovieScene3DTransformTrack>(InBinding.GetObjectGuid());
			if (Track)
			{
				bool HasActiveSection = false;

				for (const auto Section : Track->GetAllSections())
				{
					HasActiveSection = Section->IsActive();

					if (HasActiveSection) break;
				}

				TSharedPtr<FJsonObject> JsonPropertyTrack = MakeShared<FJsonObject>();
				JsonPropertyTrack->SetStringField(TEXT("Property"), TEXT("Placement"));
				JsonPropertyTrack->SetStringField(TEXT("Type"), TEXT("Transform"));
				JsonPropertyTrack->SetNumberField(TEXT("StartTime"), StartTime + InTimeOffset);
				JsonPropertyTrack->SetNumberField(TEXT("EndTime"), EndTime + InTimeOffset);
				JsonPropertyTrack->SetArrayField(TEXT("Values"), TransformTrackToJson(Track, StartTime, EndTime, InTimeOffset, ParentTransform));
				JsonPropertyTrack->SetBoolField(TEXT("Active"), HasActiveSection);
				return JsonPropertyTrack;
			}
			else
			{
				TArray<TSharedPtr<FJsonValue>> Values;
				{
					TSharedPtr<FJsonObject> Trans = TransformToJson(ActorTransform.GetTranslation(), ActorTransform.GetRotation(), ActorTransform.GetScale3D());
					Trans->SetNumberField(TEXT("Time"), InTimeOffset);
					Values.Add(MakeShared<FJsonValueObject>(Trans));
				}

				TSharedPtr<FJsonObject> JsonPropertyTrack = MakeShared<FJsonObject>();
				JsonPropertyTrack->SetStringField(TEXT("Property"), TEXT("Placement"));
				JsonPropertyTrack->SetStringField(TEXT("Type"), TEXT("Transform"));
				JsonPropertyTrack->SetNumberField(TEXT("StartTime"), StartTime + InTimeOffset);
				JsonPropertyTrack->SetNumberField(TEXT("EndTime"), EndTime + InTimeOffset);
				JsonPropertyTrack->SetArrayField(TEXT("Values"), Values);
				JsonPropertyTrack->SetBoolField(TEXT("Active"), true);
				return JsonPropertyTrack;
			}
			return nullptr;
		}

		TSharedPtr<FJsonObject> FindCameraActivityLinear(UMovieScene* InMovieScene, const FMovieSceneBinding &InBinding, float InTimeOffset)
		{
			using namespace FAGUSequencerUtils;
			using FCameraCutInfo = FExplodeCustomisations::FCameraCutInfo;

			FExplodeCustomisations* Custom = FExplodeCustomisations::Create();

			TArray<FCameraCutInfo> CameraInfos =
				Custom->GetCameraCutInfos(InMovieScene).
				FilterByPredicate([&InBinding](const FCameraCutInfo& InCameraCutInfo)
			{
				return InCameraCutInfo.CameraBinding == InBinding.GetObjectGuid();
			});

			CameraInfos.Sort([](const FCameraCutInfo& InA, const FCameraCutInfo& InB)
			{
				return InA.StartTime < InB.StartTime;
			});

			FFrameRate SequenceFrameRate = InMovieScene->GetTickResolution();

			float StartTime = SequenceFrameRate.AsSeconds(InMovieScene->GetPlaybackRange().GetLowerBoundValue());
			float EndTime = SequenceFrameRate.AsSeconds(InMovieScene->GetPlaybackRange().GetUpperBoundValue());
			float Step = 1.0f / 30.0f;

			TArray<TSharedPtr<FJsonValue>> Values;
			for (const FCameraCutInfo& CameraInfo : CameraInfos)
			{
				float CutStartTime = CameraInfo.StartTime;
				float CutEndTime = CameraInfo.EndTime;

				{
					TSharedPtr<FJsonObject> JsonTimeValue = MakeShared<FJsonObject>();
					JsonTimeValue->SetNumberField(TEXT("Time"), CutStartTime + InTimeOffset);
					JsonTimeValue->SetBoolField(TEXT("Value"), true);
					Values.Add(MakeShared<FJsonValueObject>(JsonTimeValue));
				}
				{
					TSharedPtr<FJsonObject> JsonTimeValue = MakeShared<FJsonObject>();
					JsonTimeValue->SetNumberField(TEXT("Time"), CutEndTime + InTimeOffset);
					JsonTimeValue->SetBoolField(TEXT("Value"), false);
					Values.Add(MakeShared<FJsonValueObject>(JsonTimeValue));
				}
			}

			TSharedPtr<FJsonObject> JsonPropertyTrack = MakeShared<FJsonObject>();
			JsonPropertyTrack->SetStringField(TEXT("Property"), TEXT("Activity"));
			JsonPropertyTrack->SetStringField(TEXT("Type"), TEXT("Bool"));
			JsonPropertyTrack->SetNumberField(TEXT("StartTime"), StartTime + InTimeOffset);
			JsonPropertyTrack->SetNumberField(TEXT("EndTime"), EndTime + InTimeOffset);
			JsonPropertyTrack->SetArrayField(TEXT("Values"), Values);

			delete Custom;

			return JsonPropertyTrack;
		}

		TSharedPtr<FJsonObject> AddCamera(UMovieScene* InMovieScene, const FMovieSceneBinding& InBinding, const FString& InName, float InTimeOffset = 0.0f)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();
			JsonObject->SetStringField(TEXT("Camera"), InName);
			JsonObject->SetStringField(TEXT("DisplayName"), InMovieScene->GetObjectDisplayName(InBinding.GetObjectGuid()).ToString());

			TArray<TSharedPtr<FJsonValue>> Tracks;

			// Write out the camera component tracks
			FindCameraComponentTracks(InMovieScene, InBinding, Tracks, InTimeOffset);

			// Write out tranform
			TSharedPtr<FJsonObject> JsonTransformTrack = FindCameraTransform(InMovieScene, InBinding, InTimeOffset);
			if (JsonTransformTrack.IsValid())
			{
				Tracks.Add(MakeShared<FJsonValueObject>(JsonTransformTrack));
			}

			// Write out active
			TSharedPtr<FJsonObject> JsonActivityTrack = FindCameraActivityLinear(InMovieScene, InBinding, InTimeOffset);
			if (JsonActivityTrack.IsValid())
			{
				Tracks.Add(MakeShared<FJsonValueObject>(JsonActivityTrack));
			}

			JsonObject->SetArrayField(TEXT("Tracks"), Tracks);

			return JsonObject;
		}

		class FUniqueFilenameCreator
		{
		public:
			FString			GetUniqueName(const FString& InFilename);

		private:
			TMap<FString, int>	Used;
		};

		FString FUniqueFilenameCreator::GetUniqueName(const FString& InFilename)
		{
			FString BaseName = FPaths::GetBaseFilename(InFilename).ToLower();

			int* UsedValue = Used.Find(BaseName);
			if (UsedValue)
			{
				(*UsedValue)++;
				return FString::Printf(TEXT("%s_%d"), *BaseName, *UsedValue);
			}

			Used.Add(BaseName, 1);
			return FString::Printf(TEXT("%s_1"), *BaseName);
		}

		TSharedPtr<FJsonObject> SequenceNameToJson(FSequenceNamePtr InKey)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			JsonObject->SetStringField(TEXT("Scene"), *InKey->GetScene());
			JsonObject->SetStringField(TEXT("Name"), *InKey->GetName());

			return JsonObject;
		}

		TSharedPtr<FJsonObject> RawMediaSectionToJson(FUniqueFilenameCreator& InFilenameCreator, UMovieSceneRawMediaSection* InSection, const FString& InClipType)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			FFrameRate SequenceFrameRate = InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
			float SectionStartTime = SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
			float SectionDuration = SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

			JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(InSection->GetRawMediaFilename()));
			JsonObject->SetStringField(TEXT("Type"), InClipType);
			JsonObject->SetStringField(TEXT("Media"), MakeRelToAGURoot(InSection->GetRawMediaFilename()));
			JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
			JsonObject->SetNumberField(TEXT("MediaStartTime"), InSection->GetMediaClipStartTime());
			JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
			JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());

			return JsonObject;
		}

		TSharedPtr<FJsonObject> BodyShootCutSectionToJson(FUniqueFilenameCreator& InFilenameCreator, UMovieSceneBodyShootCutSection* InSection, const FString& InClipType)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			auto Media = InSection->GetMediaFilenames();
			if (Media.Num() > 0)
			{
				TArray<TSharedPtr<FJsonValue>> MediaValues;
				for (auto Filename : Media)
				{
					MediaValues.Add(MakeShared<FJsonValueString>(MakeRelToAGURoot(Filename)));
				}

				FFrameRate SequenceFrameRate = InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
				float SectionStartTime = SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
				float SectionDuration = SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

				JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(Media[0]));
				JsonObject->SetStringField(TEXT("Type"), InClipType);
				JsonObject->SetArrayField(TEXT("Media"), MediaValues);
				JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
				JsonObject->SetNumberField(TEXT("MediaStartTime"), InSection->GetMediaClipStartTime());
				JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
				JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());
			}

			return JsonObject;
		}

		TSharedPtr<FJsonObject> SkeletalPerfCapAnimationSectionToJson(
			FUniqueFilenameCreator&					InFilenameCreator, 
			UMovieSceneSkeletalAnimationAGUSection* InSection, 
			const FString&							InClipType)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			FString Filename;
			if (InSection->Params.Animation)
			{
				UAnimSequence* AnimSequence = Cast<UAnimSequence>(InSection->Params.Animation);
				if (AnimSequence && AnimSequence->AssetImportData)
				{
					Filename = AnimSequence->AssetImportData->GetFirstFilename();
				}
				else
				{
					Filename = InSection->Params.Animation->GetName();
				}
			}

			FFrameRate SequenceFrameRate = InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
			float SectionStartTime = SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
			float SectionDuration = SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

			JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(Filename));
			JsonObject->SetStringField(TEXT("Type"), InClipType);
			JsonObject->SetStringField(TEXT("Media"), MakeRelToAGURoot(Filename));
			JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
			JsonObject->SetNumberField(TEXT("MediaStartTime"), InSection->Params.StartOffset);
			JsonObject->SetBoolField(TEXT("Looping"), InSection->Params.bLooping);
			JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
			JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());
			JsonObject->SetStringField(TEXT("SlotName"), InSection->Params.SlotName.ToString());

			TSharedPtr<FJsonObject> Transform = TransformToJson(
				InSection->Params.PlacementTransform.GetTranslation(),
				InSection->Params.PlacementTransform.GetRotation(),
				InSection->Params.PlacementTransform.GetScale3D());
			JsonObject->SetObjectField(TEXT("PlacementTransform"), Transform);

			TArray<TSharedPtr<FJsonValue>> ClipWeight = WeightToJson(InSection->Params.Weight, InSection->GetTypedOuter<UMovieScene>()->GetTickResolution());
			JsonObject->SetArrayField(TEXT("ClipWeight"), ClipWeight);
			JsonObject->SetNumberField(TEXT("PlayRate"), InSection->Params.PlayRate);
			JsonObject->SetNumberField(TEXT("EndOffset"), InSection->Params.EndOffset);

			return JsonObject;
		}

		TSharedPtr<FJsonObject> RawAudioSectionToJson(FUniqueFilenameCreator&				   InFilenameCreator,
													  UMovieSceneSection*					   InSection,
													  UMovieSceneAGUPerfCapRawAudioSubSection* InSubSection,
													  float									   InOffsetIntoMedia)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			FFrameRate SequenceFrameRate = InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
			float SectionStartTime = SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
			float SectionDuration = SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

			JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(InSubSection->GetFilePath()));
			JsonObject->SetStringField(TEXT("Type"), TEXT("Dialogue"));
			JsonObject->SetStringField(TEXT("Media"), MakeRelToAGURoot(InSubSection->GetFilePath()));
			JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
			JsonObject->SetNumberField(TEXT("MediaStartTime"), InSubSection->MediaClipStartTime + InOffsetIntoMedia);
			JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
			JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());

			return JsonObject;
		}

		TSharedPtr<FJsonObject> RawVideoSectionToJson(FUniqueFilenameCreator&				   InFilenameCreator,
													  UMovieSceneSection*					   InSection,
													  UMovieSceneAGUPerfCapRawVideoSubSection* InSubSection,
													  float									   InOffsetIntoMedia)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			FFrameRate SequenceFrameRate = InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
			float SectionStartTime = SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
			float SectionDuration = SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

			JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(InSubSection->GetFilePath()));
			JsonObject->SetStringField(TEXT("Type"), TEXT("HMC"));
			JsonObject->SetStringField(TEXT("Media"), MakeRelToAGURoot(InSubSection->GetFilePath()));
			JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
			JsonObject->SetNumberField(TEXT("MediaStartTime"), InSubSection->MediaClipStartTime + InOffsetIntoMedia);
			JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
			JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());

			return JsonObject;
		}

		TSharedPtr<FJsonObject> TrimmerSectionToJson(FUniqueFilenameCreator&				 InFilenameCreator,
													 UMovieSceneSection*					 InSection,
													 UMovieSceneAGUPerfCapTrimmerSubSection* InSubSection,
													 float									 InOffsetIntoMedia)
		{
			using namespace BodyPipelineProcess;

			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			auto Take = InSubSection->GetTake();
			if (Take.IsValid())
			{
				FString MediaFilename;
				TArray<TSharedPtr<FJsonValue>> MediaValues;

				for (FBodyShootMediaPtr Media : Take->Media)
				{
					MediaValues.Add(MakeShared<FJsonValueString>(MakeRelToAGURoot(Media->Filename)));

					if (MediaFilename.IsEmpty())
					{
						MediaFilename = Media->Filename;
					}
				}

				if (MediaFilename.IsEmpty())
				{
					ShowErrorDialog(FString::Printf(TEXT("Take %s has no valid media files; using the take name as a fallback"), *Take->Name));

					MediaFilename = Take->Name;
				}

				FFrameRate SequenceFrameRate = InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
				float SectionStartTime = SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
				float SectionDuration = SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

				JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(MediaFilename));
				JsonObject->SetStringField(TEXT("Type"), TEXT("BodyRef"));
				JsonObject->SetArrayField(TEXT("Media"), MediaValues);
				JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
				JsonObject->SetNumberField(TEXT("MediaStartTime"), InSubSection->MediaClipStartTime + InOffsetIntoMedia);
				JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
				JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());
			}

			return JsonObject;
		}

		TSharedPtr<FJsonObject> DbMediaSectionToJson(FUniqueFilenameCreator&				 InFilenameCreator,
													 UMovieSceneAGUPerfCapTrack*			 InTrack,
													 UMovieSceneSection*					 InSection,
													 UMovieSceneAGUPerfCapDbMediaSubSection* InSourceSubSection,
													 UMovieSceneAGUPerfCapGameSubSection*    InGameSubSection,
													 const FString&							 InMediaFilePath,
													 float									 InMediaOffset)
		{
			using namespace BodyPipelineProcess;

			AGUPerformanceCapture::FMovieSceneAGUPerfCapTrackEditor::FixupTrackType(InTrack);

			auto JsonObject = MakeShared<FJsonObject>();

			switch (InTrack->GetTrackType())
			{
				case EAGUPerfCapTrackType::DbAudio:
				{
					auto AudioMediaFilePath = MakeRelToAGURoot(InMediaFilePath);
					if (auto GameAudioSubSection = Cast<UMovieSceneAGUPerfCapGameAudioSubSection>(InGameSubSection))
					{
						if (auto ExternalSource = GameAudioSubSection->ExternalSource.Get())
						{
							AudioMediaFilePath = UExternalSourceBank::GetWavFilenameFromExternalSourceName(
								FPaths::GetBaseFilename(GameAudioSubSection->ExternalSource->ExternalSourcePath), TEXT("English(US)"));

							InMediaOffset = 0;
						}
					}

					JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(InMediaFilePath));
					JsonObject->SetStringField(TEXT("Type"), TEXT("Dialogue"));
					JsonObject->SetStringField(TEXT("Media"), AudioMediaFilePath);
					JsonObject->SetNumberField(TEXT("MediaStartTime"), InMediaOffset);
				}
				break;

				case EAGUPerfCapTrackType::DbFace:
				{
					JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(InMediaFilePath));
					JsonObject->SetStringField(TEXT("Type"), TEXT("HMC"));
					JsonObject->SetStringField(TEXT("Media"), MakeRelToAGURoot(InMediaFilePath));
					JsonObject->SetNumberField(TEXT("MediaStartTime"), InMediaOffset);
				}
				break;

				case EAGUPerfCapTrackType::DbBody:
				{
					auto Media = InSourceSubSection->GetCachedMedia();
					if (!Media.IsValid() || Media->GetFiles().Num() == 0)
					{
						return {};
					}

					auto MediaValues = TArray<TSharedPtr<FJsonValue>>();
					for (auto MediaFile : Media->GetFiles())
					{
						MediaValues.Add(MakeShared<FJsonValueString>(MakeRelToAGURoot(MediaFile->GetSourceFilename())));
					}

					JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(Media->GetFiles()[0]->GetSourceFilename()));
					JsonObject->SetStringField(TEXT("Type"), TEXT("BodyRef"));
					JsonObject->SetArrayField(TEXT("Media"), MediaValues);
					JsonObject->SetNumberField(TEXT("MediaStartTime"), InMediaOffset);
				}
				break;

				case EAGUPerfCapTrackType::DbCamera:
				{
					// Ignore camera track.
					return {};
				}

				default:
					return {};
					break;
			}

			const auto SequenceFrameRate	= InSection->GetTypedOuter<UMovieScene>()->GetTickResolution();
			const auto SectionStartTime		= SequenceFrameRate.AsSeconds(InSection->GetInclusiveStartFrame());
			const auto SectionDuration		= SequenceFrameRate.AsSeconds(InSection->GetRange().GetUpperBoundValue() - InSection->GetRange().GetLowerBoundValue());

			JsonObject->SetNumberField(TEXT("SequenceStartTime"), SectionStartTime);
			JsonObject->SetNumberField(TEXT("Duration"), SectionDuration);
			JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());

			return JsonObject;
		}

		TSharedPtr<FJsonObject> GamePerfCapAnimationSectionToJson(FUniqueFilenameCreator&					InFilenameCreator,
																  UMovieSceneSection*						InSection,
																  const FGuid&								InGuid,
																  UMovieSceneAGUPerfCapAnimationSubSection* InSubSection,
																  UMovieSceneAGUPerfCapDbMediaSubSection*   InSourceSubSection,
																  FAGUPerfCapAnimationSubSectionParams&		InChunk,
																  const FString&							InClipType,
																  float										InSequenceStartTime,
																  float										InMediaStartTime,
																  float										InDuration,
																  const FFrameRate&							InTickResolution)
		{
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			FString Filename;
			if (InChunk.Animation)
			{
				UAnimSequence* AnimSequence = Cast<UAnimSequence>(InChunk.Animation);
				if (AnimSequence && AnimSequence->AssetImportData)
				{
					Filename = AnimSequence->AssetImportData->GetFirstFilename();
				}
				else
				{
					Filename = InChunk.Animation->GetName();
				}
			}

			// If possible, get the section ID and the media ID so we can retrieve the path to the original source file for this animation
			FGuid	SectionId = InSection != nullptr ? InGuid : FGuid();
			FString	MediaId;

			if (InSourceSubSection)
			{
				MediaId = InSourceSubSection->MediaId;
			}


			JsonObject->SetStringField(TEXT("Uid"), InFilenameCreator.GetUniqueName(Filename));
			JsonObject->SetStringField(TEXT("Type"), InClipType);
			JsonObject->SetStringField(TEXT("Media"), MakeRelToAGURoot(Filename));
			JsonObject->SetStringField(TEXT("OriginalSource"), GetOriginalSourceAnimationFile(SectionId.ToString(EGuidFormats::Digits), MediaId, Filename));
			JsonObject->SetNumberField(TEXT("SequenceStartTime"), InSequenceStartTime);
			JsonObject->SetNumberField(TEXT("MediaStartTime"), InMediaStartTime);
			JsonObject->SetBoolField(TEXT("Looping"), InSubSection->bLooping);
			JsonObject->SetNumberField(TEXT("Duration"), InDuration);
			JsonObject->SetBoolField(TEXT("Active"), InSection->IsActive());
			JsonObject->SetStringField(TEXT("SlotName"), InSubSection->SlotName.ToString());

			TSharedPtr<FJsonObject> Transform = TransformToJson(
				InSubSection->PlacementTransform.GetTranslation(),
				InSubSection->PlacementTransform.GetRotation(),
				InSubSection->PlacementTransform.GetScale3D());
			JsonObject->SetObjectField(TEXT("PlacementTransform"), Transform);

			TArray<TSharedPtr<FJsonValue>> ClipWeight = WeightToJson(InSubSection->Weight, InTickResolution);
			JsonObject->SetArrayField(TEXT("ClipWeight"), ClipWeight);
			JsonObject->SetNumberField(TEXT("PlayRate"), InSubSection->PlayRate);

			return JsonObject;
		}

		TSharedPtr<FJsonObject> TracksToJson(FUniqueFilenameCreator& InFilenameCreator, const FString& InCharacter, UMovieScene* InMovieScene, const TArray<FTrack>& InTracks)
		{
			using namespace FacialPipelineProcess;

			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			JsonObject->SetStringField(TEXT("Character"), InCharacter);

			TArray<TSharedPtr<FJsonValue>> Clips;

			for (auto Track : InTracks)
			{
				FFrameRate SequenceFrameRate = Track.Track->GetTypedOuter<UMovieScene>()->GetTickResolution();

				FString ClipType;
				FString TrackName = Track.Track->GetDisplayName().ToString();

				if (Track.Track->IsA<UMovieSceneRawMovieTrack>() && IsFacialTrack(TrackName))						ClipType = TEXT("HMC");
				else if (Track.Track->IsA<UMovieSceneRawMovieTrack>() && IsReferenceTrack(TrackName))				ClipType = TEXT("FacialRef");
				else if (Track.Track->IsA<UMovieSceneRawAudioTrack>() && IsAudioTrack(TrackName))					ClipType = TEXT("Dialogue");
				else if (Track.Track->IsA<UMovieSceneSkeletalAnimationAGUTrack>() && IsAnimPreviewTrack(TrackName))	ClipType = TEXT("HeadAnim");
				else if (Track.Track->IsA<UMovieSceneSkeletalAnimationAGUTrack>() && TrackName == "BODY PREVIEW")	ClipType = TEXT("BodyAnim");
				else if (Track.Track->IsA<UMovieSceneSkeletalAnimationAGUTrack>() && TrackName == "AnimFinal")		ClipType = TEXT("AnimFinal");
				else if (Track.Track->IsA<UMovieSceneSkeletalAnimationAGUTrack>())									ClipType = TEXT("GeneralAnim");
				else if (Track.Track->IsA<UMovieSceneBodyShootCutTrack>())											ClipType = TEXT("BodyRef");
				else if (UMovieSceneAGUPerfCapTrack* PerfCapTrack = Cast<UMovieSceneAGUPerfCapTrack>(Track.Track))
				{
					for (auto Section : PerfCapTrack->GetAllSections())
					{
						UMovieSceneAGUPerfCapSection* PerfCapSection = nullptr;

						auto Guid = FGuid();
						auto OffsetIntoMedia = 0.0f;
						FFrameNumber FrameOffsetIntoAnimation = 0;

						if (Section->IsA<UMovieSceneAGUPerfCapSection>())
						{
							PerfCapSection = Cast<UMovieSceneAGUPerfCapSection>(Section);
							Guid = PerfCapSection->GetId();
						}
						else if (Section->IsA<UMovieSceneAGUReferenceSection>())
						{
							auto ReferenceSection = Cast<UMovieSceneAGUReferenceSection>(Section);
							Guid = ReferenceSection->GetId();

							if (ReferenceSection->GetReferencedSection())
							{
								PerfCapSection = Cast<UMovieSceneAGUPerfCapSection>(ReferenceSection->GetReferencedSection());

								if (PerfCapSection)
								{
									OffsetIntoMedia = ReferenceSection->GetPlayRangeStart() -
										MovieSceneUtils::ConvertFrameNumberToTime(
											ReferenceSection->GetReferenceSequence()->MovieScene,
											PerfCapSection->GetRange().GetLowerBoundValue());

									FrameOffsetIntoAnimation = ReferenceSection->GetPlayRangeStartFrame() - PerfCapSection->GetRange().GetLowerBoundValue();
								}
								else
								{
									UE_LOG(LogTemp, Warning, TEXT("Could not get referenced section - reference is not UMovieSceneAGUPerfCapSection"));
								}
							}
							else
							{
								UE_LOG(LogTemp, Warning, TEXT("Could not get referenced section - reference is null"));
							}
						}

						if (PerfCapSection)
						{
							if (auto RawAudioSubSection = PerfCapSection->GetSourceSubSection<UMovieSceneAGUPerfCapRawAudioSubSection>())
							{
								Clips.Add(MakeShared<FJsonValueObject>(RawAudioSectionToJson(InFilenameCreator, Section, RawAudioSubSection, OffsetIntoMedia)));
							}
							else if (auto RawVideoSubSection = PerfCapSection->GetSourceSubSection<UMovieSceneAGUPerfCapRawVideoSubSection>())
							{
								Clips.Add(MakeShared<FJsonValueObject>(RawVideoSectionToJson(InFilenameCreator, Section, RawVideoSubSection, OffsetIntoMedia)));
							}
							else if (auto TrimmerSubSection = PerfCapSection->GetSourceSubSection<UMovieSceneAGUPerfCapTrimmerSubSection>())
							{
								Clips.Add(MakeShared<FJsonValueObject>(TrimmerSectionToJson(InFilenameCreator, Section, TrimmerSubSection, OffsetIntoMedia)));
							}
							else if (auto DbMediaSubSection = PerfCapSection->GetSourceSubSection<UMovieSceneAGUPerfCapDbMediaSubSection>())
							{
								auto Clip = DbMediaSectionToJson(InFilenameCreator,
																 PerfCapTrack,
																 Section,
																 DbMediaSubSection,
																 PerfCapSection->GetGameSubSection(),
																 DbMediaSubSection->GetMediaFilePath(PerfCapSection),
																 DbMediaSubSection->GetMediaOffset(PerfCapSection) + OffsetIntoMedia);

								if (Clip.IsValid())
								{
									Clips.Add(MakeShared<FJsonValueObject>(Clip));
								}
							}

							if (auto AnimSubSection = PerfCapSection->GetGameSubSection<UMovieSceneAGUPerfCapAnimationSubSection>())
							{
								if (PerfCapTrack->IsFaceTrack())
								{
									ClipType = TEXT("HeadAnim");
								}
								else if (PerfCapTrack->IsBodyTrack())
								{
									ClipType = TEXT("BodyAnim");
								}

								if (!ClipType.IsEmpty())
								{
									auto SectionDuration = Section->GetExclusiveEndFrame() - Section->GetInclusiveStartFrame();

									FFrameNumber Offset;
									for (auto& Chunk : AnimSubSection->Chunks)
									{
										if (Chunk.Duration <= FrameOffsetIntoAnimation)
										{
											FrameOffsetIntoAnimation -= Chunk.Duration;
										}
										else
										{
											const auto StartFrame = Section->GetInclusiveStartFrame() + Offset;
											if (StartFrame >= Section->GetExclusiveEndFrame())
											{
												break;
											}

											const auto OffsetChunkDuration = Chunk.Duration - FrameOffsetIntoAnimation;
											const auto EndFrame = FMath::Min(Section->GetExclusiveEndFrame(), StartFrame + OffsetChunkDuration);

											const auto PerfCapSequenceFrameRate = PerfCapSection->GetTypedOuter<UMovieScene>()->GetTickResolution();

											const auto StartTime = PerfCapSequenceFrameRate.AsSeconds(StartFrame);
											const auto Duration = PerfCapSequenceFrameRate.AsSeconds(EndFrame - StartFrame);
											const auto MediaOffset = Chunk.StartOffset + PerfCapSequenceFrameRate.AsSeconds(FrameOffsetIntoAnimation);

											auto SourceSubSection = Cast<UMovieSceneAGUPerfCapDbMediaSubSection>(PerfCapSection->GetSourceSubSection());

											Clips.Add(MakeShared<FJsonValueObject>(GamePerfCapAnimationSectionToJson(InFilenameCreator,
												Section,
												Guid,
												AnimSubSection,
												SourceSubSection,
												Chunk,
												ClipType,
												StartTime,
												MediaOffset,
												Duration,
												PerfCapSequenceFrameRate)));

											Offset += (Chunk.Duration - FrameOffsetIntoAnimation);
											FrameOffsetIntoAnimation = 0;

											if (Offset >= SectionDuration)
											{
												break;
											}

										}
									}
								}
							}
						}
					}
					continue;
				}
				else if (auto PlacementTrack = Cast<UMovieSceneAGUAnimPlacementTrack>(Track.Track))
				{
					auto& Sections = PlacementTrack->GetAllSections();

					if (Sections.Num() > 0)
					{
						float	StartTime = SequenceFrameRate.AsSeconds(InMovieScene->GetPlaybackRange().GetLowerBoundValue());
						float	EndTime = SequenceFrameRate.AsSeconds(InMovieScene->GetPlaybackRange().GetUpperBoundValue());
						bool	HasActiveSection = false;

						for (auto Section : Sections)
						{
							HasActiveSection = HasActiveSection || (Section != nullptr && Section->IsActive());

							if (Section->HasStartFrame())
							{
								StartTime = FMath::Min<float>(StartTime, SequenceFrameRate.AsSeconds(Section->GetInclusiveStartFrame()));
							}
							if (Section->HasEndFrame())
							{
								EndTime = FMath::Max<float>(EndTime, SequenceFrameRate.AsSeconds(Section->GetExclusiveEndFrame() - 1));
							}
						}

						TSharedPtr<FJsonObject> PlacementObject = MakeShared<FJsonObject>();
						PlacementObject->SetBoolField(TEXT("Active"), Track.IsActive && HasActiveSection);
						PlacementObject->SetStringField(TEXT("Name"), TEXT("Placement"));
						PlacementObject->SetStringField(TEXT("Type"), TEXT("Transform"));
						PlacementObject->SetArrayField(TEXT("Values"), TransformTrackToJson(PlacementTrack, StartTime, EndTime));
						JsonObject->SetObjectField(TEXT("Placement"), PlacementObject);
					}

					continue;
				}

				check(!ClipType.IsEmpty());

				for (auto Section : Track.Track->GetAllSections())
				{
					if (auto MediaSection = Cast<UMovieSceneRawMediaSection>(Section))
					{
						Clips.Add(MakeShared<FJsonValueObject>(RawMediaSectionToJson(InFilenameCreator, MediaSection, ClipType)));
					}
					else if (auto AnimSection = Cast<UMovieSceneSkeletalAnimationAGUSection>(Section))
					{
						Clips.Add(MakeShared<FJsonValueObject>(SkeletalPerfCapAnimationSectionToJson(InFilenameCreator, AnimSection, ClipType)));
					}
					else if (auto BodyShootSection = Cast<UMovieSceneBodyShootCutSection>(Section))
					{
						Clips.Add(MakeShared<FJsonValueObject>(BodyShootCutSectionToJson(InFilenameCreator, BodyShootSection, ClipType)));
					}
				}
			}
			JsonObject->SetArrayField(TEXT("Clips"), Clips);

			return JsonObject;
		}

		void FindCamerasInShotTracks(TArray<TSharedPtr<FJsonValue>>& Cameras, UMovieSceneSequence* InParentSequence, UMovieSceneSequence* InFocusedSequence)
		{
			check(InParentSequence);
			check(InFocusedSequence);

			if (InParentSequence == InFocusedSequence)
			{
				return;
			}

			UMovieScene* MovieScene = InParentSequence->GetMovieScene();

			auto ProcessTrack = [&Cameras, MovieScene, InFocusedSequence](UMovieSceneTrack* InTrack)
			{
				FFrameRate SequenceFrameRate = MovieScene->GetTickResolution();

				// Only interested in direct parent sequence of the focused sequence.
				// Then we look for camera bindings.
				UMovieSceneSubTrack* SubTrack = Cast<UMovieSceneSubTrack>(InTrack);
				if (SubTrack)
				{
					for (UMovieSceneSection* Section : SubTrack->GetAllSections())
					{
						UMovieSceneSubSection* SubSection = Cast<UMovieSceneSubSection>(Section);
						if (SubSection)
						{
							if (SubSection->GetSequence() == InFocusedSequence)
							{
								// Yay! We have a hit.
								float Offset = SequenceFrameRate.AsSeconds(SubSection->Parameters.StartFrameOffset - SubSection->GetInclusiveStartFrame());

								// Look for Camera bindings.
								for (auto& Binding : MovieScene->GetBindings())
								{
									if (FMovieScenePossessable* Possessable = MovieScene->FindPossessable(Binding.GetObjectGuid()))
									{
										if (IsCameraBinding(Possessable->GetPossessedObjectClass()))
										{
											auto JsonCamera = AddCamera(MovieScene, Binding, Possessable->GetName(), Offset);
											if (JsonCamera.IsValid())
											{
												Cameras.Add(MakeShared<FJsonValueObject>(JsonCamera));
											}
										}
									}

									if (FMovieSceneSpawnable* Spawnable = MovieScene->FindSpawnable(Binding.GetObjectGuid()))
									{
										if (Spawnable->GetObjectTemplate() &&
											IsCameraBinding(Spawnable->GetObjectTemplate()->GetClass()))
										{
											auto JsonCamera = AddCamera(MovieScene, Binding, Spawnable->GetName(), Offset);
											if (JsonCamera.IsValid())
											{
												Cameras.Add(MakeShared<FJsonValueObject>(JsonCamera));
											}
										}
									}
								}
							}
							else if (SubSection->GetSequence())
							{
								// Recurse
								FindCamerasInShotTracks(Cameras, SubSection->GetSequence(), InFocusedSequence);
							}
						}
					}
				}
			};

			ProcessTrack(InParentSequence->GetMovieScene()->GetCameraCutTrack());
			for (UMovieSceneTrack* Track : MovieScene->GetMasterTracks())
			{
				ProcessTrack(Track);
			}
		}

		TSharedPtr<FJsonObject> SequenceToJson(FUniqueFilenameCreator& InFilenameCreator, FSequenceNamePtr InSequenceName, const FTransform& InTransform, UMovieSceneSequence* InRootSequence, UMovieSceneSequence* InFocusedSequence)
		{
			using namespace FacialPipelineProcess;

			TArray<TSharedPtr<FJsonValue>> Cameras;
			TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();

			UMovieScene* MovieScene = InFocusedSequence->GetMovieScene();
			FFrameRate SequenceFrameRate = MovieScene->GetTickResolution();

			JsonObject->SetObjectField(TEXT("_id"), SequenceNameToJson(InSequenceName));
			JsonObject->SetNumberField(TEXT("Duration"), SequenceFrameRate.AsSeconds(MovieScene->GetPlaybackRange().GetUpperBoundValue()));
			JsonObject->SetObjectField(TEXT("Root"), TransformToJson(InTransform.GetTranslation(), InTransform.GetRotation(), InTransform.GetScale3D()));

			TMap<FString, TArray<Internals::FTrack>> TrackMap;

			auto AddTrackToMap = [&TrackMap](const FString& InCharacter, UMovieSceneTrack* InTrack)
			{
				bool HasActiveSection = false;

				for (const auto Section : InTrack->GetAllSections())
				{
					HasActiveSection = Section->IsActive();

					if (HasActiveSection) break;
				}

				TrackMap.FindOrAdd(InCharacter).Add({ InTrack, HasActiveSection });
			};

			for (auto Track : MovieScene->GetMasterTracks())
			{
				FString Character;
				FString TrackName = Track->GetDisplayName().ToString();

				if (auto MovieTrack = Cast<UMovieSceneRawMovieTrack>(Track))
				{
					if (IsFacialTrack(TrackName, &Character))
					{
						AddTrackToMap(Character, MovieTrack);
					}
					else if (IsReferenceTrack(TrackName))
					{
						AddTrackToMap(TEXT("Reference"), MovieTrack);
					}
				}
				else if (auto AudioTrack = Cast<UMovieSceneRawAudioTrack>(Track))
				{
					if (IsAudioTrack(TrackName, &Character))
					{
						AddTrackToMap(Character, AudioTrack);
					}
				}
			}

			for (auto& Binding : MovieScene->GetBindings())
			{
				if (FMovieScenePossessable* Possessable = MovieScene->FindPossessable(Binding.GetObjectGuid()))
				{
					if (IsCameraBinding(Possessable->GetPossessedObjectClass()))
					{
						auto JsonCamera = AddCamera(MovieScene, Binding, Possessable->GetName());
						if (JsonCamera.IsValid())
						{
							Cameras.Add(MakeShared<FJsonValueObject>(JsonCamera));
						}
					}

					for (auto Track : Binding.GetTracks())
					{
						if (auto AnimTrack = Cast<UMovieSceneSkeletalAnimationAGUTrack>(Track))
						{
							AddTrackToMap(Possessable->GetName(), AnimTrack);
						}
						else if (auto BodyShootTrack = Cast<UMovieSceneBodyShootCutTrack>(Track))
						{
							AddTrackToMap(Possessable->GetName(), BodyShootTrack);
						}
						else if (auto AnimPlacementTrack = Cast<UMovieSceneAGUAnimPlacementTrack>(Track))
						{
							AddTrackToMap(Possessable->GetName(), AnimPlacementTrack);
						}
						else if (auto PerfCapTrack = Cast<UMovieSceneAGUPerfCapTrack>(Track))
						{
							AddTrackToMap(Possessable->GetName(), PerfCapTrack);
						}
					}
				}

				if (FMovieSceneSpawnable* Spawnable = MovieScene->FindSpawnable(Binding.GetObjectGuid()))
				{
					if (Spawnable->GetObjectTemplate() &&
						IsCameraBinding(Spawnable->GetObjectTemplate()->GetClass()))
					{
						auto JsonCamera = AddCamera(MovieScene, Binding, Spawnable->GetName());
						if (JsonCamera.IsValid())
						{
							Cameras.Add(MakeShared<FJsonValueObject>(JsonCamera));
						}
					}
				}
			}

			FindCamerasInShotTracks(Cameras, InRootSequence, InFocusedSequence);

			TArray<TSharedPtr<FJsonValue>> Tracks;
			for (auto& Pair : TrackMap)
			{
				Tracks.Add(MakeShared<FJsonValueObject>(TracksToJson(InFilenameCreator, Pair.Key, MovieScene, Pair.Value)));
			}

			JsonObject->SetArrayField(TEXT("Tracks"), Tracks);
			JsonObject->SetArrayField(TEXT("Cameras"), Cameras);

			return JsonObject;
		}

		static bool Export(const FString& InFilename, TSharedPtr<FJsonObject> InJsonObject)
		{
			if (FPaths::FileExists(InFilename))
			{
				FScopedSlowTask SlowTask(2.f);

				SlowTask.MakeDialog();
				SlowTask.EnterProgressFrame(1.f, FText::FromString(FString::Printf(TEXT("Checking out file %s"), *InFilename)));

				if (SourceControlHelpers::CheckOutOrAddFile(InFilename))
				{
					// Wait for read only flag to clear
					SlowTask.EnterProgressFrame(1.f, FText::FromString(FString::Printf(TEXT("Waiting for file %s"), *InFilename)));

					constexpr auto MaxWaitTimeSeconds = 10;

					for (auto WaitTime = 0; WaitTime < MaxWaitTimeSeconds && IFileManager::Get().IsReadOnly(*InFilename); ++WaitTime)
					{
						FPlatformProcess::Sleep(1.f);
					}
				}

				if (IFileManager::Get().IsReadOnly(*InFilename))
				{
					UE_LOG(LogAGUArtGraphicUtils, Warning, TEXT("Failed to check out file %s"), *InFilename);

					return false;
				}

				SlowTask.EnterProgressFrame(0.f, FText::FromString(FString::Printf(TEXT("Checked out file %s"), *InFilename)));
			}

			FString data;
			auto writer = TJsonWriterFactory<TCHAR>::Create(&data);
			FJsonSerializer::Serialize(InJsonObject.ToSharedRef(), writer);
			FFileHelper::SaveStringToFile(data, *InFilename, FFileHelper::EEncodingOptions::ForceUTF8);

			SourceControlHelpers::MarkFileForAdd(InFilename);

			return true;
		}

		static bool Export(const TCHAR* InFilename, const FTransform& InTransform, UMovieSceneSequence* InRootSequence, UMovieSceneSequence* InFocusedSequence)
		{
			Internals::FUniqueFilenameCreator UniqueFilenameCreator;

			FString SequenceName = FPaths::GetBaseFilename(InFilename);
			FString Scene, Name;

			if (SequenceName.Split(TEXT("_"), &Scene, &Name))
			{
				FSequenceNamePtr SequenceNamePtr = MakeShared<FSequenceName>();
				SequenceNamePtr->SetScene(Scene);
				SequenceNamePtr->SetName(Name);

				TArray<TSharedPtr<FJsonValue>> Items;
				Items.Add(MakeShared<FJsonValueObject>(Internals::SequenceToJson(UniqueFilenameCreator, SequenceNamePtr, InTransform, InRootSequence, InFocusedSequence)));

				TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();
				JsonObject->SetArrayField(TEXT("Sequences"), Items);
				return Internals::Export(InFilename, JsonObject);
			}
			else
			{
				FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(FString::Printf(TEXT(
					"%s: unable to find scene & sequence name - please rename the sequence to have an underscore character between the scene name and the sequence name"), *SequenceName)));
			}

			return false;
		}
	}

	void ExportSequenceDescription(TSharedPtr<ISequencer> InSequencer)
	{
		UMovieSceneSequence* MasterSequence = InSequencer->GetRootMovieSceneSequence();
		UMovieSceneSequence* Sequence = InSequencer->GetFocusedMovieSceneSequence();
		ALevelSequenceActorAGU* InActor = Cast<ALevelSequenceActorAGU>(InSequencer->GetActorAsOrigin());
		ULevelSequenceAGU* SequenceAGU = Cast<ULevelSequenceAGU>(Sequence);
		
		FString ActName = SequenceAGU->GetAct();
		FString SceneName =  SequenceAGU->GetScene();
		FString SequenceName = SequenceAGU->GetSequence();
		FString WorkfilesPath = FString(TEXT("%AGURoot%/Projects")) / FApp::GetProjectName() / TEXT("WorkFiles");
		FString SequencePath = FString(TEXT("SequenceData")) / ActName / SceneName / SequenceName;
		FString Filename = WorkfilesPath / SequencePath / FPaths::GetBaseFilename(Sequence->GetOutermost()->GetName()) + TEXT(".json");
		FPlatformMisc::ExpandEnvironmentStr(*Filename, Filename);

		FTransform Trans = FTransform::Identity;
		if (InActor && InActor->GetUseLevelSequenceActorAsOrigin())
		{
			Trans = InActor->GetActorTransform();
		}

		if (Internals::Export(*Filename, Trans, MasterSequence, Sequence))
		{
			// Mark Sequence as exported for future lock checks
			auto& SequenceFlags = SequenceAGU->GetSequenceFlags();
			SequenceFlags.bAnimationExported = true;

			SequenceAGU->MarkPackageDirty();
		}
	}
}
