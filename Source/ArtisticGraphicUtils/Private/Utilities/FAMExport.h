// Created by kukki, 07 July 2021.
#pragma once
#include "Templates/SharedPointer.h"

class ISequencer;

namespace FAMExport
{
	void ExportSequenceDescription(TSharedPtr<ISequencer> InSequencer);
}
