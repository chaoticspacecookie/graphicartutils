// Created by kukki, 07 July 2021.

#pragma once
#include "AGUArtGraphicUtilsSettings.generated.h"

UCLASS(config = Engine, defaultconfig)
class AGUTECHARTTOOLS_API UAGUArtGraphicUtilsSettings : public UObject
{
GENERATED_BODY()
public:

	static UAGUArtGraphicUtilsSettings const* Get() { return GetDefault<UAGUArtGraphicUtilsSettings>(); }
	
	/** Array of Python scripts to run at start-up (run after the Engine has initialized - PostEngineInit). */
	UPROPERTY(config, EditAnywhere, Category=Python, meta=(ConfigRestartRequired=true, MultiLine=true))
	TArray<FString> StartupScripts;
};