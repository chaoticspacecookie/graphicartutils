// Created by kukki, 07 July 2021.
#pragma once
#include "CoreMinimal.h"
#include "Logging/LogMacros.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

AGUTECHARTTOOLS_API DECLARE_LOG_CATEGORY_EXTERN(LogAGUArtGraphicUtils, Log, All);

class AGUTECHARTTOOLS_API IAGUArtGraphicUtils : public IModuleInterface
{
public:

	static FORCEINLINE IAGUArtGraphicUtils& Get()
	{
		return FModuleManager::LoadModuleChecked<IAGUArtGraphicUtils>("AGUArtGraphicUtils");
	}

	static FORCEINLINE bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("AGUArtGraphicUtils");
	}
};
