// Created by kukki, 07 July 2021.
#pragma once
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TextureStats.h"
#include "AGUTextureStats.generated.h"

class UTexture;
class UAGUTextureStatsData;

// Python Example:
// import unreal
// print unreal.AGUTextureStats.get_texture_stats_by_package_name("/Game/VFX/ParticleSystems/Generic/Rain/Textures/TC_Act1_Kidnap_BlurSky").get_type()
UCLASS(Const, Transient, Category = "AGUArtGraphicUtils")
class AGUTECHARTTOOLS_API UAGUTextureStats : public UBlueprintFunctionLibrary
{
GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, Category = "Textures")
	static UAGUTextureStatsData* GetTextureStatsByPackageName(FString InPackageName);

	UFUNCTION(BlueprintCallable, Category = "Textures")
	static UAGUTextureStatsData* GetTextureStatsByObjectPath(FString InObjectPath);

	UFUNCTION(BlueprintCallable, Category = "Textures")
	static UAGUTextureStatsData* GetTextureStatsByStringReference(FSoftObjectPath InReference);

	UFUNCTION(BlueprintCallable, Category = "Textures")
	static UAGUTextureStatsData* GetTextureStats(UTexture* InTexture);
};

UCLASS(Transient, Category = "AGUArtGraphicUtils")
class AGUTECHARTTOOLS_API UAGUTextureStatsData : public UTextureStats
{
GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, Category = "Stats")
	UTexture* GetTexture() const;

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE FString GetType() const { return Type; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE FVector2D GetMaxDimensions() const { return MaxDim; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE FVector2D GetCurrentDimensions() const { return CurrentDim; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE EPixelFormat GetFormat() const { return Format; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE TextureGroup GetGroup() const { return Group; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE int32 GetLODBias() const { return LODBias; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE float GetCurrentKB() const { return CurrentKB; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE float GetFullyLoadedKB() const { return FullyLoadedKB; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE float GetLastTimeRendered() const { return LastTimeRendered; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	FORCEINLINE FString GetPath() const { return Path; }
};
